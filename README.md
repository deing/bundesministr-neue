# Bundesministr Neue

## Purpose

Bundesministr Neue (trademark registration pending) is a total re-write of my simple discord bot based on [serenity](https://crates.io/crates/serenity) that I was writing as an exercise. It's home page is at https://15318.de/bundesministr.

## Features

As of v1.x, Bundesministr Neue can
* Execute some commands of minor usefulness
* Mirror all messages from a single channel per guild into another
* Support multiple languages — shipped with this repo are German and English messages.

## Requirements

- SQLite 3
- nano (the setup script assumes its presence)

## Building

### Development

1. Create a fork to (optionally) contribute your changes from, the clone that fork to your device using `git clone git@gitlab.com:yourusernamehere/bundesministr-neue`.
2. Run `git remote add upstream git@gitlab.com:deing/bundesministr-neue` to allow you to pull in new changes with `git pull upstream master`.
3. Run `setup.sh` from the root project folder, it'll set up everything for you.

### Production
1. Download the newest release from https://gitlab.com/deing/bundesministr-neue/tags, these are commits at which the repo is in a moderately stable state. For now, new Features and important fixes are designated `1.X` versions, while minor fixes are given `1.X.X` version numbers.
2. Run `setup.sh` from the root project folder, it'll set up everything for you.
3. Run `cargo build --release` to obtain an optimized build, which will be located in `target/release/bundesministr-neue`.
