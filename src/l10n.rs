use serenity::client::Context;
use serenity::model::channel::Message;
use std::collections::BTreeMap;
use toml;
use toml::value::{Table, Value};

type StringTable = BTreeMap<String, Table>;

pub struct BundesministrL10NProvider(StringTable);

impl BundesministrL10NProvider {
	pub fn from_toml(input: &str) -> Self {
		let toml: Table = toml::from_str(input).unwrap();
		let langs: Table = toml["language"].clone().try_into().unwrap();
		let mut out: StringTable = BTreeMap::new();
		for t in langs {
			out.insert(t.0.to_string(), t.1.try_into().unwrap());
		}
		BundesministrL10NProvider(out)
	}
	pub fn translate(&self, lang: &str, key: &str) -> String {
		if !self.0.contains_key(lang) {
			return format!("Unknown language `{}`", lang);
		}
		self.0[lang]
			.get(key)
			.unwrap_or(&Value::String(format!("Unknown key `{}`", key)))
			.to_string()
			.replace("\"", "")
			.replace("\\n", "\n")
	}
	pub fn list_langs(&self) -> Vec<String> {
		self.0.keys().map(|x| x.to_owned()).collect()
	}
	pub fn has_lang(&self, lang: &str) -> bool {
		self.0.contains_key(lang)
	}
}

pub fn text(ctx: &Context, msg: &Message, key: &str, ins: Option<&[String]>) -> String {
	let data = ctx.data.lock();
	let lang = if msg.guild_id.is_none() {
		"en"
	} else if let Some(guild_info) = data
		.get::<crate::data::GuildInfos>()
		.unwrap()
		.get(&msg.guild_id.unwrap().0)
	{
		&guild_info.lang
	} else {
		"en"
	};
	let mut str = data
		.get::<crate::data::l10n>()
		.unwrap()
		.translate(lang, key);

	if ins.is_some() {
		for insert in ins.unwrap() {
			str = str.replacen("%%", insert, 1);
		}
	}
	str
}
