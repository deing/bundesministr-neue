use crate::commands::{CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;

pub fn fancy_subcommands(headline: &str, subcommands: &[&str]) -> crate::commands::CommandResult {
	crate::commands::CommandResult::Text(format!(
		"__{}__\n**Subcommands:** {}",
		headline,
		subcommands
			.iter()
			.fold(String::new(), |acc, s| format!("{} `{}`", acc, s))
	))
}
pub fn ok_user_error(
	ctx: &Context,
	msg: &Message,
	key: &str,
	ins: Option<&[String]>,
) -> CommandReturnType {
	Ok(CommandResult::UserError(crate::l10n::text(
		ctx, msg, key, ins,
	)))
}
pub fn ok_command_error(why: &str) -> CommandReturnType {
	Ok(CommandResult::CommandError(why.to_string()))
}
