use log::*;
use serenity::client::Client;
use simplelog::{Config, TermLogger};
use std::collections::{BTreeMap, HashMap};
use std::fs;
use toml;

const CONFIG_FILE_LOCATION: &str = "config.toml";
const L10N_FILE_LOCATION: &str = "l10n.toml";
pub const CONFIG_DB_LOCATION: &str = "config.sqlite";

pub mod bundesministr;
pub mod commands;
pub mod data;
pub mod helpers;
pub mod l10n;

use crate::bundesministr::*;

fn main() {
	TermLogger::init(LevelFilter::Info, Config::default()).unwrap();
	let cfg = load_config(CONFIG_FILE_LOCATION);
	let (command_execs, command_data) = commands::load_command_register();
	let l10n = load_l10n(L10N_FILE_LOCATION);
	let config_conn = load_db(CONFIG_DB_LOCATION);
	start_client(&cfg, command_execs, command_data, l10n, &config_conn);
}

struct Configuration {
	secrets: BTreeMap<String, String>,
	public: BTreeMap<String, String>,
}

fn start_client(
	cfg: &Configuration,
	command_execs: HashMap<String, commands::CommandExecuteFunction>,
	command_data: HashMap<String, commands::Command>,
	l10n: l10n::BundesministrL10NProvider,
	config_conn: &rusqlite::Connection,
) {
	info!("Appointing Bundesministr…");
	let mut client = Client::new(&cfg.secrets["client_token"], BundesministrHandler)
		.expect("main.rs→start_client failed to create client");
	client.with_framework(BundesministrFramework {
		prefix: cfg.public["prefix"].to_string(),
		operator: cfg.public["operator"].parse().unwrap(),
		command_execs,
		command_data,
	});
	{
		let mut data = client.data.lock();
		data.insert::<data::l10n>(l10n);
		data.insert::<data::GuildInfos>(load_guild_config_db(&config_conn));
		data.insert::<data::MirroringData>(load_mirroring_db(&config_conn));
		data.insert::<data::BundesData>(data::BundesministrData::default());
	}
	client
		.start()
		.expect("main.rs→start_client failed to start client");
}

fn load_config(path: &str) -> Configuration {
	let config_content =
		fs::read_to_string(path).expect("main.rs→load_config failed to read config file");
	let config_data: BTreeMap<String, BTreeMap<String, String>> =
		toml::from_str(&config_content).expect("main.rs→config failed to parse config file");
	Configuration {
		secrets: config_data["secrets"].clone(),
		public: config_data["public"].clone(),
	}
}

fn load_l10n(path: &str) -> l10n::BundesministrL10NProvider {
	let l10n_content = fs::read_to_string(path).expect("main.rs→load_10n failed to read l10n file");
	l10n::BundesministrL10NProvider::from_toml(&l10n_content)
}

fn load_db(path: &str) -> rusqlite::Connection {
	rusqlite::Connection::open(path)
		.unwrap_or_else(|_| panic!("main.rs→load_db failed to load DB {}", path))
}

fn load_guild_config_db(conn: &rusqlite::Connection) -> HashMap<u64, data::GuildInfo> {
	let mut guild_config_db = conn
		.prepare("SELECT * from guild_config")
		.expect("main.rs→load_guild_config_db failed to load guild_config table");
	let mut guild_config_db = guild_config_db.query(rusqlite::NO_PARAMS).unwrap();
	let mut guild_config: HashMap<u64, data::GuildInfo> = HashMap::new();
	while let Some(row) = guild_config_db.next() {
		let row = row.unwrap();
		guild_config.insert(
			row.get::<&str, i64>("guild_id") as u64,
			data::GuildInfo {
				lang: row.get("lang"),
			},
		);
	}
	guild_config
}

fn load_mirroring_db(conn: &rusqlite::Connection) -> HashMap<u64, u64> {
	let mut mirroring_db = conn
		.prepare("SELECT * from mirroring")
		.expect("main.rs→load_mirroring_db failed to load mirroring table");
	let mut mirroring_db = mirroring_db.query(rusqlite::NO_PARAMS).unwrap();
	let mut mirroring: HashMap<u64, u64> = HashMap::new();
	while let Some(row) = mirroring_db.next() {
		let row = row.unwrap();
		mirroring.insert(
			row.get::<&str, i64>("in_id") as u64,
			row.get::<&str, i64>("out_id") as u64,
		);
	}
	mirroring
}
