use serenity::client::Context;
use serenity::model::channel::Message;
use std::collections::HashMap;

pub type CommandReturnType = Result<CommandResult, Box<dyn std::error::Error>>;

pub type CommandExecuteFunction = fn(&[String], &Context, &Message) -> CommandReturnType;

#[derive(Debug)]
pub enum CommandResult {
	// Plain Text to reply with
	Text(String),
	// Error that the user can fix
	UserError(String),
	// Internal command error, external module errors should be propagated with ? instead of returning an Ok(CommandResult)
	CommandError(String),
	// File from /assets/ to upload and message content
	File(String, String),
	// Silent Success, don't send anything
	Success,
}

#[derive(Debug, Copy, Clone)]
pub enum Category {
	Core,
	Governance,
	Util,
	Memes,
}
pub const NUM_CATEGORIES: usize = 4;

pub struct Command {
	pub required_perms: u64,
	pub category: Category,
	pub guild_only: bool,
}

mod about;
mod config;
mod debug;
mod help;
mod kat;
mod leo;
mod ping;
mod roll;
// mod ultrathonk;

pub fn load_command_register() -> (
	HashMap<String, CommandExecuteFunction>,
	HashMap<String, Command>,
) {
	let command_names = vec![
		"ping",
		"dbg",
		"help",
		"about",
		"kat",
		"leo",
		"roll",
		"config",
		// "ultrathonk",
	];
	let command_execs: Vec<CommandExecuteFunction> = vec![
		ping::execute,
		debug::execute,
		help::execute,
		about::execute,
		kat::execute,
		leo::execute,
		roll::execute,
		config::execute,
		// ultrathonk::execute,
	];
	let command_data = vec![
		ping::PING,
		debug::DEBUG,
		help::HELP,
		about::ABOUT,
		kat::KAT,
		leo::LEO,
		roll::ROLL,
		config::CONFIG,
		// ultrathonk::ULTRATHONK,
	];

	(
		command_names
			.iter()
			.map(|x| x.to_string())
			.zip(command_execs)
			.collect(),
		command_names
			.iter()
			.map(|x| x.to_string())
			.zip(command_data)
			.collect(),
	)
}
