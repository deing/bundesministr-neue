use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use crate::helpers::*;
use serenity::client::Context;
use serenity::model::channel::Message;

pub const DEBUG: Command = Command {
	required_perms: 2048,
	category: Category::Core,
	guild_only: false,
};

pub fn execute(args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	let arg0 = args.get(0);
	if arg0.is_none() {
		return Ok(fancy_subcommands(
			"dbg",
			&["fail_int", "fail_ext", "fail_usr", "l10n"],
		));
	}
	match arg0.unwrap().to_lowercase().as_str() {
		"fail_ext" => {
			let _no = "blob".parse::<i64>()?;
			Ok(CommandResult::Success)
		}
		"fail_int" => ok_command_error("Successfully failed to execute."),
		"fail_usr" => ok_user_error(ctx, msg, "e_args", None),
		"l10n" => {
			let arg1 = args.get(1);
			if let Some(key) = arg1 {
				let out = crate::l10n::text(ctx, msg, &key, None);
				Ok(CommandResult::Text(format!(
					"__localizing:__ `{}`\n{}",
					key, out
				)))
			} else {
				Ok(CommandResult::UserError(
					"Supply a translation key.".to_string(),
				))
			}
		}
		_ => Ok(CommandResult::Success),
	}
}
