use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;

pub const ABOUT: Command = Command {
	required_perms: 2048,
	category: Category::Core,
	guild_only: false,
};

pub fn execute(_args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	let (version, num_guilds);
	{
		let data = ctx.data.lock();
		version = data
			.get::<crate::data::BundesData>()
			.unwrap()
			.version
			.to_owned();
		num_guilds = data
			.get::<crate::data::BundesData>()
			.unwrap()
			.num_guilds
			.to_string();
	}
	Ok(CommandResult::Text(crate::l10n::text(
		ctx,
		msg,
		"about",
		Some(&[version, num_guilds]),
	)))
}
