use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use rand::Rng;
use regex::Regex;
use serenity::client::Context;
use serenity::model::channel::Message;

pub const ROLL: Command = Command {
	required_perms: 2048,
	category: Category::Util,
	guild_only: false,
};

pub fn execute(args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	if args.is_empty() {
		return Ok(CommandResult::UserError(crate::l10n::text(
			ctx, msg, "e_args", None,
		)));
	}
	let mut rng = rand::thread_rng();
	let mut out = String::new();
	let matcher = Regex::new(r"^[0-9]+d[0-9]+\s?[+-]?\s?[0-9]*$")?;
	for arg in args.iter() {
		if !matcher.is_match(arg) {
			return Ok(CommandResult::UserError(crate::l10n::text(
				ctx,
				msg,
				"e_roll_malformed",
				Some(&[arg.to_owned()]),
			)));
		}
		let (count, sides, modifier) = parse(&arg);
		if sides == 0 || count == 0 {
			return Ok(CommandResult::UserError(crate::l10n::text(
				ctx,
				msg,
				"e_roll_malformed",
				Some(&[arg.to_owned()]),
			)));
		}
		let total = rng.gen_range(count, sides * count + 1) + modifier;
		out += format!(" {}: {} |", arg, total).as_str();
	}
	out.pop();
	out = out.trim().to_string();
	Ok(CommandResult::Text(out))
}

fn parse(input: &str) -> (isize, isize, isize) {
	let input = input.replace(' ', "");
	let split: Vec<&str> = input.split(|c| c == 'd' || c == '+' || c == '-').collect();
	let mut count = split.get(0).unwrap_or(&"0").parse().unwrap_or_default();
	let sides = split.get(1).unwrap_or(&"0").parse().unwrap_or_default();
	let mut modifier = split.get(2).unwrap_or(&"0").parse().unwrap_or_default();
	if input.contains('-') {
		modifier *= -1;
	}
	// Sanity check: Prevent overflow, later panic in RNG when attempting to roll stupidly large dice
	if count * sides > 200_000 || isize::abs(modifier) > 200_000 {
		// This will be caught in the main execute() and cause an error
		count = 0;
	}
	(count, sides, modifier)
}
