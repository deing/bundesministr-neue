use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;

pub const HELP: Command = Command {
	required_perms: 2048,
	category: Category::Core,
	guild_only: false,
};

// Category Discriminator, Category Variant Name, Names of commands in that category
type CategoryCommandCompound = (usize, String, String);

pub fn execute(_args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	let mut output = String::new();
	let mut compounds: Vec<CategoryCommandCompound> = vec![];
	for category in 0..crate::commands::NUM_CATEGORIES {
		compounds.push((category, String::new(), String::new()));
	}
	let (_, command_descs) = crate::commands::load_command_register();
	for (name, desc) in command_descs {
		let index = desc.category as usize;
		compounds[index].1 = format!("{:?}", desc.category);
		compounds[index].2 += &format!("`{}` ", name);
	}
	compounds.iter().for_each(|c| {
		if c.1.is_empty() {
			return;
		}
		output += &format!("**{}:** {}\n", c.1, c.2)
	});
	Ok(CommandResult::Text(crate::l10n::text(
		ctx,
		msg,
		"help",
		Some(&[output]),
	)))
}
