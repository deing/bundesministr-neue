use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;

pub const KAT: Command = Command {
	required_perms: 2048,
	category: Category::Memes,
	guild_only: false,
};

pub fn execute(_args: &[String], _ctx: &Context, _msg: &Message) -> CommandReturnType {
	Ok(CommandResult::File(
		"kat.png".to_string(),
		"<@371151824331210755>".to_string(),
	))
}
