use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::misc::Mentionable;

pub const PING: Command = Command {
	required_perms: 2048,
	category: Category::Util,
	guild_only: false,
};

pub fn execute(_args: &[String], _ctx: &Context, msg: &Message) -> CommandReturnType {
	Ok(CommandResult::Text(format!(
		"{} Pong!",
		msg.author.mention()
	)))
}
