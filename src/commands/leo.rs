use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use crate::helpers::*;
use serenity::client::Context;
use serenity::model::channel::Message;
use std::collections::HashMap;

pub const LEO: Command = Command {
	required_perms: 2048,
	category: Category::Util,
	guild_only: false,
};

pub fn execute(args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	if args.is_empty() {
		return ok_user_error(ctx, msg, "e_args", None);
	}
	let mut map = HashMap::new();
	map.insert("url", args[0].clone());
	let http_client = reqwest::Client::new();
	let res = http_client
		.post("https://leo.immobilien/api/create")
		.json(&map)
		.send();
	if res.is_ok() {
		let res: LeoResponse = res?.json()?;
		if res.status == "success" {
			Ok(CommandResult::Text(format!(
				"https://leo.immobilien/{}",
				res.urlKey
			)))
		} else {
			ok_command_error(&res.status)
		}
	} else {
		ok_command_error("Failed to connect to leo.immobilien API.")
	}
}

#[derive(serde::Deserialize)]
#[allow(non_snake_case)]
struct LeoResponse {
	status: String,
	urlKey: String,
}
