use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use crate::helpers::*;
use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::misc::Mentionable;

pub const CONFIG: Command = Command {
	required_perms: 32,
	category: Category::Core,
	guild_only: true,
};

pub fn execute(args: &[String], ctx: &Context, msg: &Message) -> CommandReturnType {
	if args.is_empty() {
		Ok(crate::helpers::fancy_subcommands(
			"cfg",
			&["lang", "mirror"],
		))
	} else {
		match args[0].to_lowercase().as_str() {
			"lang" => {
				if args.len() == 1 {
					list_langs(ctx, msg)
				} else {
					update_lang(ctx, msg, args)
				}
			}
			"mirror" => {
				if args.len() == 1 {
					Ok(CommandResult::UserError(crate::l10n::text(
						ctx, msg, "e_args", None,
					)))
				} else {
					mirror(ctx, msg, args)
				}
			}
			_ => Ok(CommandResult::UserError(crate::l10n::text(
				ctx,
				msg,
				"e_unknown_subcommand",
				None,
			))),
		}
	}
}

fn list_langs(ctx: &Context, msg: &Message) -> CommandReturnType {
	let langs = ctx
		.data
		.lock()
		.get::<crate::data::l10n>()
		.unwrap()
		.list_langs()
		.iter()
		.fold(String::new(), |acc, x| format!("{}\n‣ {}", acc, x));
	Ok(CommandResult::Text(crate::l10n::text(
		ctx,
		msg,
		"config_lang_list",
		Some(&[langs]),
	)))
}

fn update_lang(ctx: &Context, msg: &Message, args: &[String]) -> CommandReturnType {
	let has_lang;
	{
		let data = ctx.data.lock();
		let l10n = data.get::<crate::data::l10n>().unwrap();
		has_lang = l10n.has_lang(&args[1]);
	}
	if !has_lang {
		Ok(CommandResult::UserError(crate::l10n::text(
			ctx,
			msg,
			"e_config_lang_unknown",
			None,
		)))
	} else {
		{
			let mut data = ctx.data.lock();
			let guild_configs = data.get_mut::<crate::data::GuildInfos>().unwrap();
			guild_configs.insert(
				msg.guild_id.unwrap().0,
				crate::data::GuildInfo {
					lang: args[1].to_owned(),
				},
			);
			let config_conn = rusqlite::Connection::open(crate::CONFIG_DB_LOCATION);
			if let Ok(conn) = config_conn {
				if conn
					.execute(
						"INSERT INTO guild_config (guild_id, lang) VALUES (?,?)",
						&[msg.guild_id.unwrap().0.to_string(), args[1].to_owned()],
					)
					.is_err()
				{
					return ok_command_error(
						"Failed execute database insert. The configuration will be lost on restart.",
					);
				}
			} else {
				return ok_command_error("Failed to connect to database.");
			}
		}
		Ok(CommandResult::Text(crate::l10n::text(
			ctx,
			msg,
			"g_welcome",
			None,
		)))
	}
}

fn mirror(ctx: &Context, msg: &Message, args: &[String]) -> CommandReturnType {
	if let Ok(id) = args[1].replace("<#", "").replace(">", "").parse::<u64>() {
		if id == msg.channel_id.0 {
			return Ok(CommandResult::UserError(crate::l10n::text(
				ctx,
				msg,
				"e_config_mirror_invalid_channel",
				None,
			)));
		}
		let channel = serenity::model::id::ChannelId::from(id);
		if channel
			.say(crate::l10n::text(
				ctx,
				msg,
				"config_mirror_start",
				Some(&[msg.channel_id.mention()]),
			))
			.is_err()
		{
			ok_command_error("Failed to send message in target channel")
		} else {
			{
				let mut data = ctx.data.lock();
				let mirroring = data.get_mut::<crate::data::MirroringData>().unwrap();
				mirroring.insert(msg.channel_id.0, id);
			}
			let config_conn = rusqlite::Connection::open(crate::CONFIG_DB_LOCATION).unwrap();
			config_conn
				.execute(
					"INSERT INTO mirroring (guild_id, in_id, out_id) VALUES (?, ?, ?)",
					&[
						msg.guild_id.unwrap().to_string(),
						msg.channel_id.to_string(),
						id.to_string(),
					],
				)
				.unwrap();
			Ok(CommandResult::Success)
		}
	} else if args[1].to_lowercase() == "stop" {
		{
			let mut data = ctx.data.lock();
			let mirroring = data.get_mut::<crate::data::MirroringData>().unwrap();
			mirroring.remove(&msg.channel_id.0);
		}
		let config_conn = rusqlite::Connection::open(crate::CONFIG_DB_LOCATION).unwrap();
		config_conn
			.execute(
				"DELETE FROM mirroring WHERE guild_id=?",
				&[msg.guild_id.unwrap().to_string()],
			)
			.unwrap();
		Ok(CommandResult::Text(crate::l10n::text(
			ctx,
			msg,
			"config_mirror_stop",
			None,
		)))
	} else {
		Ok(CommandResult::UserError(crate::l10n::text(
			ctx,
			msg,
			"e_config_mirror_invalid_channel",
			None,
		)))
	}
}
