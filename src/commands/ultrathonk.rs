use crate::commands::{Category, Command, CommandResult, CommandReturnType};
use serenity::client::Context;
use serenity::model::channel::Message;

pub const ULTRATHONK: Command = Command {
	required_perms: 2048,
	category: Category::Memes,
	guild_only: false,
};

pub fn execute(_args: &[String], _ctx: &Context, _msg: &Message) -> CommandReturnType {
	Ok(CommandResult::File(
		"ultrathonk.png".to_string(),
		String::new(),
	))
}
