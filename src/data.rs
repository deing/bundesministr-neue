use std::collections::HashMap;
/// Structs for storage in the Client's `data` sharemap or databases
use typemap::Key;

#[allow(non_camel_case_types)]
pub struct l10n;
impl Key for l10n {
	type Value = crate::l10n::BundesministrL10NProvider;
}

pub struct GuildInfo {
	pub lang: String,
}

pub struct GuildInfos;
impl Key for GuildInfos {
	type Value = HashMap<u64, GuildInfo>;
}

pub struct MirroringData;
impl Key for MirroringData {
	type Value = HashMap<u64, u64>;
}

pub struct BundesData;
impl Key for BundesData {
	type Value = BundesministrData;
}

pub struct BundesministrData {
	pub num_guilds: usize,
	pub version: String,
}
impl Default for BundesministrData {
	fn default() -> Self {
		Self {
			num_guilds: 0,
			version: env!("CARGO_PKG_VERSION").to_string(),
		}
	}
}
