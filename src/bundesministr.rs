use crate::commands::CommandResult;
use log::*;
use serenity::{
	client::{Context, EventHandler},
	framework::Framework,
	model::{channel::Message, gateway::Ready, guild::Guild, guild::GuildStatus},
};
use std::collections::HashMap;
use threadpool::ThreadPool;

pub struct BundesministrHandler;

impl EventHandler for BundesministrHandler {
	fn message(&self, ctx: Context, msg: Message) {
		let data = ctx.data.lock();
		let mirroring_data = data.get::<crate::data::MirroringData>().unwrap();
		if mirroring_data.contains_key(&msg.channel_id.0) {
			let channel =
				serenity::model::id::ChannelId::from(*mirroring_data.get(&msg.channel_id.0).unwrap());
			let mut content = msg.content;
			content = content.replace("`", "\u{200B}`\u{200B}"); // Escape backticks
			content.truncate(1920);
			let out = format!(
				"__{}__ ({})```{} ```",
				msg.author.tag(),
				msg.timestamp,
				content
			);
			if let Err(e) = channel.say(out) {
				warn!(
					"Error mirroring message {} from {} to {}: {}",
					msg.id, msg.channel_id, channel, e
				);
				return;
			}
			debug!(
				"Successfully mirrored message {} from {} to {}",
				msg.id, msg.channel_id, channel
			);
		}
	}
	fn ready(&self, ctx: Context, data_about_bot: Ready) {
		let mut data = ctx.data.lock();
		{
			data
				.get_mut::<crate::data::BundesData>()
				.unwrap()
				.num_guilds = data_about_bot.guilds.len();
		}
		info!(
			"Bundesministr v{} ready",
			data.get::<crate::data::BundesData>().unwrap().version
		);
		info!(
			"Serving {} guilds: {:?}",
			data_about_bot.guilds.len(),
			data_about_bot
				.guilds
				.iter()
				.map(|x| match x {
					GuildStatus::OnlineGuild(g) => g.name.to_owned(),
					GuildStatus::OnlinePartialGuild(g) => g.name.to_owned(),
					GuildStatus::Offline(g) => g.id.to_partial_guild().unwrap().name.to_owned(),
				})
				.collect::<Vec<String>>()
		);
	}
	fn guild_create(&self, ctx: Context, guild: Guild, is_new: bool) {
		if is_new {
			info!(
				"Joined new guild {} (owner is {}, {} members)",
				guild.name,
				guild.owner_id.to_user().unwrap().tag(),
				guild.member_count
			);
			let mut data = ctx.data.lock();
			data
				.get_mut::<crate::data::BundesData>()
				.unwrap()
				.num_guilds += 1;
		}
	}
}

pub struct BundesministrFramework {
	pub prefix: String,
	pub operator: u64,
	pub command_execs: HashMap<String, crate::commands::CommandExecuteFunction>,
	pub command_data: HashMap<String, crate::commands::Command>,
}

impl Framework for BundesministrFramework {
	fn dispatch(&mut self, ctx: Context, msg: Message, tp: &ThreadPool) {
		if msg.author.bot || msg.kind != serenity::model::channel::MessageType::Regular {
			return;
		}
		let parsed = parse_command(&self.prefix, &msg.content);
		if parsed.is_none() {
			return;
		}
		let (command, mut arguments) = parsed.unwrap();
		info!(
			"{} issued {} with arguments {:?}",
			msg.author.tag(),
			command,
			arguments
		);
		if !self.command_execs.contains_key(&command) {
			warn!("Unknown command.");
			return;
		}
		#[allow(clippy::clone_on_copy)]
		// clone is needed here to avoid possible lifetime issues with the function pointer
		let exec = self.command_execs[&command].clone();
		let mut allowed = self.check_permissions(self.command_data[&command].required_perms, &msg);
		if msg.content.ends_with(" !") && msg.author.id.0 == self.operator {
			allowed = true;
			arguments.pop();
			warn!("Operator permission check override");
		}
		let guild_only = self.command_data[&command].guild_only;
		tp.execute(move || {
			let delivery_success;
			if guild_only && msg.guild_id.is_none() {
				delivery_success = msg
					.channel_id
					.say("__error__\nThis command can only be used in a guild.");
			} else if !allowed {
				delivery_success = msg
					.channel_id
					.say("__error__\nYou have insufficient permissions to execute this.");
			} else {
				let result = exec(&arguments, &ctx, &msg);
				if let Err(t) = result {
					error!("Command failed (propagated): {:?}", t);
					delivery_success = msg
						.channel_id
						.say(crate::l10n::text(&ctx, &msg, "e_external", None));
				} else {
					match result.unwrap() {
						CommandResult::Text(t) => {
							info!("Successfully executed.");
							delivery_success = msg.channel_id.say(t);
						}
						CommandResult::Success => {
							info!("Successfully executed.");
							return;
						}
						CommandResult::File(file, t) => {
							info!("Successfully executed.");
							delivery_success = msg
								.channel_id
								.send_files(vec![format!("assets/{}", file).as_str()], |m| m.content(t));
						}
						CommandResult::UserError(t) => {
							warn!("Command returned user error: {}", t);
							delivery_success =
								msg
									.channel_id
									.say(crate::l10n::text(&ctx, &msg, "e_user", Some(&[t])));
						}
						CommandResult::CommandError(t) => {
							warn!("Command failed (internal): {}", t);
							delivery_success =
								msg
									.channel_id
									.say(crate::l10n::text(&ctx, &msg, "e_internal", Some(&[t])));
						}
					}
				}
			}
			if let Err(e) = delivery_success {
				warn!("Failed to deliver message: {:?}", e);
			} else {
				debug!("Delivered Message #{}", delivery_success.unwrap().id);
			}
		});
	}
}

impl BundesministrFramework {
	pub fn check_permissions(&self, perms: u64, message: &Message) -> bool {
		let member = message.member();
		if member.is_none() {
			return true;
		}
		let author_perms = message.guild().unwrap().read().members[&message.author.id]
			.permissions()
			.unwrap();
		author_perms.contains(serenity::model::permissions::Permissions::from_bits_truncate(perms))
	}
}

fn parse_command(prefix: &str, message: &str) -> Option<(String, Vec<String>)> {
	if message.len() < prefix.len() + 1 || !message.starts_with(prefix) {
		None
	} else {
		let args: Vec<String> = message.split_whitespace().map(|x| x.to_string()).collect();
		let command = args[0].get(prefix.len()..).unwrap().to_string();
		if args.len() == 1 {
			Some((command, vec![]))
		} else {
			let arguments = args[1..].iter().map(|x| x.to_string()).collect();
			Some((command, arguments))
		}
	}
}
#[test]
fn _t_parse_command() {
	assert_eq!(
		parse_command(".", ".help"),
		Some(("help".to_string(), Vec::new()))
	);
	assert_eq!(parse_command(".", "blob"), None);
	assert_eq!(
		parse_command(".", ".help a     b"),
		Some(("help".to_string(), vec!["a".to_string(), "b".to_string()]))
	);
}
