#!/bin/bash
echo [setup] Welcome $(whoami)!
echo [setup] Fetching dependencies and building…
cargo build
echo [setup] Setting up SQLite Tables…
sqlite3 config.sqlite "CREATE TABLE guild_config (guild_id STRING UNIQUE ON CONFLICT REPLACE, lang STRING NOT NULL)"
sqlite3 config.sqlite "CREATE TABLE mirroring (guild_id STRING UNIQUE ON CONFLICT REPLACE, in_id STRING NOT NULL, out_id STRING NOT NULL)"
read -sr -n 1 -p "[setup] Press any key to continue to [nano] for editing your configuration. Exit with Ctrl-S Ctrl-X."
echo [secrets] >> config.toml
echo client_token = \"your-discord-bot-token\" >> config.toml
echo >> config.toml
echo [public] >> config.toml
echo prefix = \".\" >> config.toml
echo operator = \"your-discord-id\" >> config.toml
sleep .1
nano config.toml
sleep .1
echo [setup] If no errors occurred, you should now be able to run [cargo run] to start your own version of Bundesministr!
echo [setup] Best of luck!